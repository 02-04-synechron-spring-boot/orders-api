package com.example.demo.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {

    private final RestTemplate restTemplate;

    @GetMapping
    public Flux<String> allUsers(){
        WebClient webClient = WebClient.builder().baseUrl("https://jsonplaceholder.typicode.com").build();

        Flux<String> response = webClient
                                .get()
                                .uri("/users")
                                .retrieve()
                                .bodyToFlux(String.class);

        /*String response = this.restTemplate.getForObject("https://jsonplaceholder.typicode.com/users", String.class); */
        return response;

    }
}
