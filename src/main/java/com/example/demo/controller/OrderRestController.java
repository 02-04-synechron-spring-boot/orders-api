package com.example.demo.controller;

import com.example.demo.model.Order;
import com.example.demo.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.Set;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
@Slf4j
public class OrderRestController {

    private final OrderService orderService;

    @GetMapping(produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public Map<String, Object> fetchAllOrders(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "10") int size,
            @RequestParam(name = "order", required = false, defaultValue = "asc") String direction,
            @RequestParam(name = "field", required = false, defaultValue = "customerName") String property){

        log.info("Came inside the fetchAllOrders method::");
        return this.orderService.fetchAllOrders(page, size, direction, property);
    }

    @GetMapping("/{id}")
    public Order fetchOrderById(@PathVariable("id") long orderId){
        return this.orderService.fetchOrderById(orderId);
    }

   @GetMapping("/price")
    public Map<String, Object> fetchOrderByPriceRange(
           @RequestParam(name = "page", required = false, defaultValue = "0") int page,
           @RequestParam(name = "size", required = false, defaultValue = "10") int size,
           @RequestParam(name = "order", required = false, defaultValue = "asc") String direction,
           @RequestParam(name = "field", required = false, defaultValue = "customerName") String property,
           @RequestParam(name = "min", required = true) double min,
           @RequestParam(name = "max", required = true) double max
   ){
       Sort.Direction sortDirection = direction.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
       PageRequest pageRequest = PageRequest.of(page, size, sortDirection, property);
       return this.orderService.fetchAllOrdersByPriceRange(min, max, pageRequest);
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public Order saveOrder(@RequestBody  @Valid Order order){
        return this.orderService.save(order);
    }

    @PutMapping("/{id}")
    public Order updateOrder(@PathVariable("id") long orderId, @RequestBody Order order){
        return this.orderService.updateOrder(orderId, order);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteOrderById(@PathVariable("id") long orderId){
        this.orderService.deleteOrderById(orderId);
    }
}
