package com.example.demo.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error handleInvalidOrder(Exception exception){
        return new Error(100, exception.getMessage());
    }
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Object> handleInvalidArguments(MethodArgumentNotValidException exception){
        int errorCount = exception.getErrorCount();
        List<ObjectError> allErrors = exception.getAllErrors();
        Set<String> errorMessages = allErrors
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toSet());
        LinkedHashMap<String, Object> errorResultMap = new LinkedHashMap<>();
        errorResultMap.put("error", errorCount);
        errorResultMap.put("messages", errorMessages);
        return errorResultMap;
    }
}

@RequiredArgsConstructor
@Getter
class Error {
    private final int code;
    private final String message;
}
