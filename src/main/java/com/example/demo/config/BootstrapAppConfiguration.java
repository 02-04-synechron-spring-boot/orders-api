package com.example.demo.config;

import com.example.demo.model.Customer;
import com.example.demo.model.LineItem;
import com.example.demo.model.Order;
import com.example.demo.model.Role;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.repository.OrderRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Configuration
@RequiredArgsConstructor
@Profile("dev")
public class BootstrapAppConfiguration {

    private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;
    private final PasswordEncoder passwordEncoder;

    @Value("${app.limit}")
    private int limit;
    private Faker faker = new Faker();

    @EventListener
    public void onEvent(ApplicationReadyEvent readyEvent){
        IntStream
                .rangeClosed(0, limit)
                .forEach(index -> {
                    Order order = Order
                                    .builder()
                                        .customerName(faker.name().fullName())
                                        .orderTime(faker.date().past(6, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
                                        .price(faker.number().randomDouble(2, 4000, 8000))
                                        .email(faker.internet().emailAddress().toLowerCase())
                                    .build();
                    IntStream.rangeClosed(1, faker.number().numberBetween(2, 4))
                            .forEach(value -> {
                                LineItem lineItem = LineItem.builder()
                                                                .name(faker.commerce().productName())
                                                                .qty(faker.number().numberBetween(2,5))
                                                                .price(faker.number().randomDouble(2, 400, 800))
                                                            .build();
                                order.addLineItem(lineItem);
                            });
                    this.orderRepository.save(order);
                });
    }

    @EventListener
    public void bootstrapApplicationUsers(ApplicationReadyEvent readyEvent){
        Customer user = Customer.builder()
                                    .name("kiran")
                                    .password(passwordEncoder.encode("welcome"))
                .emailAddress("kiran@gmail.com")
                                .build();
        Customer admin = Customer.builder()
                                    .name("vinay")
                                    .password(passwordEncoder.encode("welcome"))
                .emailAddress("vinay@gmail.com")
                                .build();

        Role userRole =  Role.builder().role("USER").build();
        Role adminRole =  Role.builder().role("ADMIN").build();

        user.addRole(userRole);

        admin.addRole(userRole);
        admin.addRole(adminRole);

        this.customerRepository.save(user);
        this.customerRepository.save(admin);
    }
}
