package com.example.demo.config;


import com.example.demo.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
class DBHealthcheckIndicator implements HealthIndicator{

    private final OrderRepository orderRepository;

    @Override
    public Health health() {
        try {
            long count = this.orderRepository.count();
            return Health.up().withDetail("DB-Service", "DB-service is up").build();
        } catch (Exception exception){
            return Health.down().withDetail("DB-Service", exception.getMessage()).build();
        }
    }
}

@Configuration
class KafkaHealthcheckIndicator implements HealthIndicator{

    @Override
    public Health health() {
            return Health.up().withDetail("Kafka-Service", "Kafka-service is up").build();
    }
}
