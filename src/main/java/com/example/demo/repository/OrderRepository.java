package com.example.demo.repository;

import com.example.demo.dto.OrderDto;
import com.example.demo.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    Page<OrderDto> findByPriceBetween(double min, double max, Pageable page);

    Page<OrderDto> findByPriceLessThan(double maxPrice, Pageable pageable);

    @Query("select order from Order order where order.email = ?1")
    Optional<Order> findByEmail(String emailAdderss);
}
