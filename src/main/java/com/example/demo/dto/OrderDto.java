package com.example.demo.dto;

import java.time.LocalDateTime;

public interface OrderDto {

    String getCustomerName() ;

    String getEmail();

    double getPrice();

    LocalDateTime getOrderTime();
}
