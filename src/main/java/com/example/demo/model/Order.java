package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.sound.sampled.Line;
import javax.validation.Valid;
import javax.validation.constraints.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name="orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long orderId;
    @NotBlank(message = "customer name cannot be empty")
    private String customerName;
    @Email(message = "email is not in correct format")
    private String email;
    @DecimalMin(value = "4000", message = "order price should be of min 4k" )
    @DecimalMax(value = "25000", message = "order price cannot be more than 25k" )
    private double price;
    @PastOrPresent(message = "Order date cannot be in the future")
    private LocalDateTime orderTime;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonManagedReference
    @Valid
    private Set<LineItem> lineItems;

    public void addLineItem(LineItem lineItem){
        if(this.lineItems == null){
            this.lineItems = new HashSet<>();
        }
        this.lineItems.add(lineItem);
        lineItem.setOrder(this);
    }
}
