package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="roles")
@NoArgsConstructor
@Data
@EqualsAndHashCode(exclude = "customers")
@ToString(exclude = "customers")
@Builder
@AllArgsConstructor
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String role;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "customers_roles",
            joinColumns = @JoinColumn(name="role_id"),
            inverseJoinColumns = @JoinColumn(name="customer_id")
    )
    @JsonIgnore
    private Set<Customer> customers;

    public Set<Customer> getCustomers(){
        if (this.customers == null){
            this.customers = new HashSet<>();
        }
        return this.customers;
    }
}
