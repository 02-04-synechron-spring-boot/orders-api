package com.example.demo.service;

import com.example.demo.dto.OrderDto;
import com.example.demo.model.Order;
import com.example.demo.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.common.reflection.XProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;

    public Order save(Order order){
        return this.orderRepository.save(order);
    }

    public Map<String, Object> fetchAllOrders(int page, int size, String direction, String property){
        Sort.Direction sortDirection = direction.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        PageRequest pageRequest = PageRequest.of(page, size, sortDirection, property);
        Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);

        //process the response
        long totalRecords = pageResponse.getTotalElements();
        int totalPages = pageResponse.getTotalPages();
        int currentPageNumber = pageResponse.getNumber();
        int recordsPerPage = pageResponse.getNumberOfElements();
        List<Order> data = pageResponse.getContent();

        LinkedHashMap<String, Object> responseMap = new LinkedHashMap<>();
        responseMap.put("total", totalRecords);
        responseMap.put("pages", totalPages);
        responseMap.put("page", currentPageNumber);
        responseMap.put("size", recordsPerPage);
        responseMap.put("data", data);

        return responseMap;
    }

    public Map<String, Object> fetchAllOrdersByPriceRange(double min, double max, PageRequest pageRequest){
        Page<OrderDto> pageResponse = this.orderRepository.findByPriceBetween(min, max, pageRequest);

        //process the response
        long totalRecords = pageResponse.getTotalElements();
        int totalPages = pageResponse.getTotalPages();
        int currentPageNumber = pageResponse.getNumber();
        int recordsPerPage = pageResponse.getNumberOfElements();
        List<OrderDto> data = pageResponse.getContent();

        LinkedHashMap<String, Object> responseMap = new LinkedHashMap<>();
        responseMap.put("total", totalRecords);
        responseMap.put("pages", totalPages);
        responseMap.put("page", currentPageNumber);
        responseMap.put("size", recordsPerPage);
        responseMap.put("data", data);

        return responseMap;
    }

    public Order fetchOrderById(long orderId){
        return this.orderRepository
                        .findById(orderId)
                        .orElseThrow(() -> new IllegalArgumentException("Invalid order id passed"));
    }

    public Order updateOrder(long orderId, Order order){
        return this.orderRepository.save(order);
    }

    public void deleteOrderById(long orderId){
        this.orderRepository.deleteById(orderId);
    }
}
