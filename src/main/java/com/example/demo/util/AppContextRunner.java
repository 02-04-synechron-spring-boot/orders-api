package com.example.demo.util;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class AppContextRunner implements CommandLineRunner {

    private final ApplicationContext applicationContext;

    public AppContextRunner(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }


    @Override
    public void run(String... args) throws Exception {
        Arrays
                .asList(this.applicationContext.getBeanDefinitionNames())
                .stream()
                    .filter(this::contains)
                    .forEach(System.out::println);
    }

    public boolean contains(String bean){
        return bean.contains("user");
    }
}
