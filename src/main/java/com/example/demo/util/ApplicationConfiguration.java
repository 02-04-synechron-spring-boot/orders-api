package com.example.demo.util;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    @Bean
    @ConditionalOnProperty(prefix = "app", value = "loadBean", havingValue = "true")
    public User user(){
        return new User();
    }

    @Bean
    @ConditionalOnBean(name = "user")
    public User userBasedOnBeanCondition(){
        return new User();
    }
    @Bean
    @ConditionalOnMissingBean(name = "user")
    public User userBasedOnMissingBeanCondition(){
        return new User();
    }
}

class User {

}
