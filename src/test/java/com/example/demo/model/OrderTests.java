package com.example.demo.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class OrderTests {

    @Test
    public void testConstructor(){
        Order order = Order.builder().orderTime(LocalDateTime.now()).orderId(23L).email("kiran@gmail.com").customerName("Vinod").build();

        assertNotNull(order);
        assertEquals(23L, order.getOrderId());
        assertEquals("Vinod", order.getCustomerName());
        assertEquals("kiran@gmail.com", order.getEmail());
    }
    @Test
    public void testAddLineItems(){
        Order order = Order.builder().orderTime(LocalDateTime.now()).orderId(23L).email("kiran@gmail.com").customerName("Vinod").build();
        LineItem lineItem = LineItem.builder().price(2500).qty(2).name("Leather bag").build();
        order.addLineItem(lineItem);

        //assertions
        Set<LineItem> lineItems = order.getLineItems();
        Assertions.assertNotNull(lineItems);
        assertEquals(1, lineItems.size());
        assertTrue(lineItems.contains(lineItem));
        assertTrue(lineItem.getOrder() == order);


    }
}
