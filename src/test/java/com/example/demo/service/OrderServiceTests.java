package com.example.demo.service;

import com.example.demo.model.Order;
import com.example.demo.repository.OrderRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTests {

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderService orderService;



    @Test
    public void testSaveOrder(){
        //expectations on the mock object
        Order order = Order.builder().orderId(1111L).orderTime(LocalDateTime.now()).email("test@gmail.com").customerName("Vikram").price(25000).build();
        when(this.orderRepository.save(any(Order.class))).thenReturn(order);
        //execution
        Order savedOrder = this.orderService.save(order);
        //assertions
        assertEquals(savedOrder.getOrderId(), 1111L);
        assertEquals(savedOrder.getEmail(), "test@gmail.com");
        //verification
        verify(this.orderRepository, Mockito.times(1) ).save(order);
    }

    @Test
    public void deleteOrderById(){

        //expectations
        doAnswer(invocation -> {
            Assertions.assertEquals((long)invocation.getArgument(0), 22L );
            return null;
        }).when(this.orderRepository).deleteById(22L);

        this.orderService.deleteOrderById(22);

        verify(this.orderRepository, times(1)).deleteById(22L);

        //Person person = new Person(22, 33,"Vinod", "Ramesh" , true, true);
    }

    @Test
    void testFetchById(){
        //expectations on the mock object
        Order order = Order.builder().orderId(1111L).orderTime(LocalDateTime.now()).email("test@gmail.com").customerName("Vikram").price(25000).build();
        when(this.orderRepository.findById(anyLong())).thenReturn(Optional.of(order));
        //execution
        Order fetchedOrder = null;
        try {
            fetchedOrder = this.orderService.fetchOrderById(22L);
            //assertions
            assertEquals(fetchedOrder.getOrderId(), 1111L);
            assertEquals(fetchedOrder.getEmail(), "test@gmail.com");
        }catch (Exception exception){
            fail("This method should not throw any exception");
        }
        //verification
        verify(this.orderRepository, Mockito.times(1) ).findById(anyLong());
    }

    @Test
    void testFetchByInvalidId(){
        //expectations on the mock object
        Order order = Order.builder().orderId(1111L).orderTime(LocalDateTime.now()).email("test@gmail.com").customerName("Vikram").price(25000).build();
        when(this.orderRepository.findById(anyLong())).thenReturn(Optional.ofNullable(null));
        //execution
        try {
            Order fetchedOrder = this.orderService.fetchOrderById(22L);
            fail("Should throw exception");
        } catch (Exception exception){
          Assertions.assertNotNull(exception);
          Assertions.assertTrue(exception instanceof IllegalArgumentException);
        }
        //verification
        verify(this.orderRepository, Mockito.times(1) ).findById(anyLong());
    }

    @Test
    void testFetchOrderByInvalidId(){
        //expectations on the mock object
        Order order = Order.builder().orderId(1111L).orderTime(LocalDateTime.now()).email("test@gmail.com").customerName("Vikram").price(25000).build();
        when(this.orderRepository.findById(anyLong())).thenReturn(Optional.ofNullable(null));
        assertThrows(IllegalArgumentException.class, () ->  this.orderService.fetchOrderById(1111L));
        verify(this.orderRepository, Mockito.times(1) ).findById(anyLong());
    }

    @Test
    void testFetchOrderByValidId(){
        //expectations on the mock object
        Order order = Order.builder().orderId(1111L).orderTime(LocalDateTime.now()).email("test@gmail.com").customerName("Vikram").price(25000).build();
        when(this.orderRepository.findById(anyLong())).thenReturn(Optional.ofNullable(order));
        assertDoesNotThrow(() ->  this.orderService.fetchOrderById(1111L), "This method should not throw an exception");
        verify(this.orderRepository, Mockito.times(1) ).findById(anyLong());
    }
}
